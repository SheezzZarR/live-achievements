from django.apps import AppConfig


class LiveAchievementsMainConfig(AppConfig):
    name = 'LiveAchievements_main'
