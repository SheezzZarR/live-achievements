from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static
from django.contrib.auth.decorators import login_required
from LiveAchievements import settings


urlpatterns = [
    path('', login_required(views.IndexPage.as_view()), name='home'),

    path('accounts/profile/', login_required(views.ProfilePage.as_view()), name='profile'),
    path('accounts/<str:username>/', login_required(views.PersonProfilePage.as_view())),
    path('accounts/profile/change/me/', views.change_profile, name='account_change_info'),
    path('accounts/<str:username>/add/', views.add_friend),
    path('accounts/<str:username>/remove/', views.remove_friend),
    path('top/users', views.top_users, name='top_ladder'),

    path('top/achievements', views.top_achievements, name='top_achievements'),
    path('achievements/create/', login_required(views.AchievementCreationPage.as_view()), name='create'),
    path('achievements/list/', login_required(views.AchievementListPage.as_view()), name='achievements'),
    path('achievements/list/<str:caption>/', login_required(views.AchievementCheckPage.as_view())),
    path('achievements/<str:caption>/change/<str:commentary>/', views.change_comment),
    path('achievements/list/<str:caption>/change/', login_required(views.AchievementChangePage.as_view())),

    path('attempt/<str:caption>/', login_required(views.AttemptCreationPage.as_view())),

    path('reports/review/', login_required(views.ReportPage.as_view()), name='reports'),

    path('about/developers/', login_required(views.AboutPage.as_view()), name='about')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()

