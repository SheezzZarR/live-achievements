from django.contrib.auth.models import User
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from itertools import chain
from operator import attrgetter

# Create your models here.


class Reports(models.Model):
    """ A model that describes a report """
    from_who = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")
    what_am_i = models.CharField(max_length=15)
    message = models.CharField(max_length=510)


class AdditionalInfo(models.Model):
    """ Model that describes user"s info"""
    user = models.ForeignKey(to=User, on_delete=models.DO_NOTHING, blank=False, null=True)
    exp = models.PositiveIntegerField(default=0, blank=True)
    img = models.ImageField(upload_to="avatars/", blank=True)
    status = models.CharField(max_length=150, default="", blank=True)
    warnings = models.PositiveIntegerField(default=0)
    reports = GenericRelation(to=Reports, related_query_name="Reports")

    @property
    def get_activity(self):
        """ A function that returns activity of the user for given quantity """
        achievements = Achievement.objects.filter(creator=self.user)
        attempts = Attempt.objects.filter(creator=self.user)
        ach_comments = AchievementRate.objects.filter(creator=self.user)
        attempt_comments = AttemptRate.objects.filter(creator=self.user)
        ach_completed = AchievementGot.objects.filter(user=self.user)

        activity = sorted(
            chain(achievements, attempts, ach_comments, attempt_comments, ach_completed),
            key=attrgetter("date")
        )

        return activity

    @property
    def get_warnings(self):
        """ A function that returns all warnings received by a user """
        warns = ReportWarning.objects.filter(user=self.user)

        return warns


class Achievement(models.Model):
    """ Model that describes an achievement """
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=True)
    creator_info = models.ForeignKey(to=AdditionalInfo, on_delete=models.CASCADE, blank=True, null=True)
    caption = models.CharField(max_length=60)
    description = models.CharField(max_length=300)
    date = models.DateField(auto_now=True)
    difficulty = models.PositiveIntegerField(default=1, blank=False)
    me = models.CharField(max_length=15, default="achievement", blank=True)
    exp = models.IntegerField(default=100, blank=True)
    photo = models.ImageField(upload_to="achievements_photos/", blank=True, null=True)
    reports = GenericRelation(to=Reports, related_query_name="Reports")

    @property
    def rate(self):
        """ Function that finds rating of the achievement """
        a = AchievementRate.objects.filter(achievement=self)
        if len(a) == 0:
            return -1
        return sum(list(map(lambda a: a.rate, a))) / len(a)

    class Meta:
        ordering = ["-id"]


class AchievementRate(models.Model):
    """ Model that describes rates of the achievement"""
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=True)
    achievement = models.ForeignKey(to=Achievement, on_delete=models.CASCADE, blank=True, null=True)
    commentary = models.CharField(max_length=300, blank=True, null=True)
    date = models.DateField(auto_now=True)
    me = models.CharField(max_length=15, default="ach_rate", blank=True)


class AchievementGot(models.Model):
    """ Model that describes users who has finished the achievement """
    achievement = models.ForeignKey(to=Achievement, on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateField(auto_now=True)

    class Meta:

        ordering = ["-id"]


class AchievementsTag(models.Model):
    """ Model that describes tags of the achievement * CURRENTLY NOT USED * """
    achievement = models.ForeignKey(to=Achievement, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=30)
    date = models.DateField(auto_now=True)
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=True)


class Attempt(models.Model):
    """ Model that describes user"s attempt """
    achievement = models.ForeignKey(to=Achievement, on_delete=models.CASCADE, blank=True, null=True)
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=True)
    creator_info = models.ForeignKey(to=AdditionalInfo, on_delete=models.CASCADE, blank=True, null=True)
    is_completed = models.BooleanField(default=False)
    me = models.CharField(max_length=15, default="attempt", blank=True)
    date = models.DateField(auto_now=True)
    photo = models.ImageField(upload_to="attempt_photos/before/", blank=True, null=True)
    photo_second = models.ImageField(upload_to="attempt_photos/after/", blank=True, null=True)
    reports = GenericRelation(to=Reports, related_query_name="Reports")

    @property
    def rate(self):
        """ Function that finds rating of the attempt """
        a = AttemptRate.objects.filter(attempt=self)
        if len(a) == 0:
            return -1
        return round(sum(list(map(lambda a: a.rate, a))) / len(a) * 100)

    @property
    def all_rates_by_users(self):
        rates = AttemptRate.objects.filter(attempt=self)
        return list(map(lambda a: str(a.creator) + " " + str(a.rate), rates))

    @property
    def get_comments(self):
        comments = AttemptRate.objects.filter(attempt=self)

        return comments

    @property
    def get_todo_list(self):
        todos = AttemptTodoList.objects.filter(attempt=self)
        output = []

        for i in todos:
            output.append(i.step)

        return output

    class Meta:
        ordering = ["-id"]


class AttemptTodoList(models.Model):
    """ A model that describes a step for completion of the achievement """
    attempt = models.ForeignKey(to=Attempt, on_delete=models.CASCADE, blank=True, null=True)
    step = models.CharField(max_length=100)


class AttemptRate(models.Model):
    """ Model that describes rating of the attempt"""
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=True)
    attempt = models.ForeignKey(to=Attempt, on_delete=models.CASCADE, blank=True, null=True)
    rate = models.IntegerField(default=-1)
    commentary = models.CharField(max_length=500, blank=True, null=True)
    date = models.DateField(auto_now=True)
    me = models.CharField(max_length=15, default="attempt_rate", blank=True)
    reports = GenericRelation(to=Reports, related_query_name="Reports")


class ReportWarning(models.Model):
    """ A models that describes a warn for a user """
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=True)
    reason = models.CharField(max_length=510, blank=True, null=True)


class FriendShip(models.Model):
    from_friend = models.ForeignKey(to=User, related_name="friend_set", on_delete=models.DO_NOTHING)
    to_friend = models.ForeignKey(to=User, related_name="to_friend_set", on_delete=models.DO_NOTHING)
    is_sub = models.BooleanField(default=False)
    is_friend = models.BooleanField(default=False)

    def __unicode__(self):
        return u"%s, %s" % (
            self.from_friend.username,
            self.to_friend.username
        )

    class Meta:
        unique_together = (("to_friend", "from_friend"),)
