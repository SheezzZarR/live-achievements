from . import models
from os import getcwd, remove
from django.contrib.auth.models import User
from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile

import unittest
from datetime import datetime


class AchievementTestCase(TestCase):
    """ Class to test achievement's model """

    def setUp(self):
        """ Preparations """
        user = User.objects.create(username="TestUser", password="KeKus333", email="qwerty123@mail.com")

        photo = SimpleUploadedFile(
            name='test_image.png',
            content=open('./images_for_test/models/test_image.png', 'rb').read(),
            content_type='image/png'
        )

        models.Achievement.objects.create(
            creator=user,
            caption="Test achievement",
            description="This is a test. I hope it'll pass",
            date=datetime.now(),
            exp=500,
            photo=photo
        )

        models.AchievementRate.objects.create(
            creator=user,
            achievement=models.Achievement.objects.get(id=1),
            rate=5,
            commentary="test",
            date=datetime.now()
        )

    def test_achievement_can_be_created(self):
        """ Test to verify that achievement's are correct """
        achievement = models.Achievement.objects.get(id=1)
        user = User.objects.get(id=1)
        achievement_rating = achievement.rate

        self.assertEqual(achievement.creator, user)
        self.assertEqual(achievement.caption, "Test achievement"),
        self.assertEqual(achievement.description, "This is a test. I hope it'll pass"),
        self.assertEqual(achievement.exp, 500),
        self.assertEqual(achievement.photo.name, 'achievements_photos/test_image.png')
        self.assertEqual(achievement_rating, 5)

    def tearDown(self):
        """ Deletion of needless stuff """
        remove('./media/achievements_photos/test_image.png')
