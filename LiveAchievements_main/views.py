from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db import OperationalError
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from django.views import View

from . import forms
from . import models
from . import validators


# Create your views here.

# https://docs.djangoproject.com/en/2.2/topics/auth/default/#topic-authorization


# TODO: MESSAGES
# TODO: TOP
# TODO: ACCESSIBILITY OF THE REPORTS (any user can type reports/review/ )


class IndexPage(View):
    """ A  class that is responsible for the feed page"""
    login_required = True

    def get(self, request):
        """ A function that processes get requests on the feed page """
        check_user(request)

        try:
            attempts = models.Attempt.objects.filter(is_completed=True)
        except ObjectDoesNotExist:
            attempts = []

        else:
            if "report" in request.GET.keys():
                report_form = forms.ReportForm()

                return render(request, 'snippets/send_abuse.html', {'form': report_form})

            elif "comment" in request.GET.keys():
                attempt_comment_form = forms.AttemptRateForm()

                return render(request, 'snippets/send_attempt_comment.html', {'form': attempt_comment_form})

        try:
            avatar = models.AdditionalInfo.objects.get(user=request.user)
        except (ObjectDoesNotExist, OperationalError):
            avatar = None

        content = {
            'user': request.user,
            'avatar': avatar,
            'attempts': attempts,
            'page_name': "Лента попыток"
        }
        return render(request, 'index.html', content)

    def post(self, request):
        """ A function that processes post requests on the feed page """
        if 'attempt_id' in request.POST.keys() and len(request.POST) == 2:
            process_attempt_delete(request)

            return redirect(reverse('home'))

        elif 'message' in request.POST.keys() and 'obj_type' in request.POST.keys():
            process_report(request)

            return redirect(reverse('home'))

        elif 'message' in request.POST.keys() and 'attempt_id' in request.POST.keys():
            process_attempt_comment(request, request.POST['achievement_id'])

            return redirect(reverse('home'))

        elif 'type' and 'action' and 'id' in request.POST.keys() and len(request.POST) == 4:
            process_attempt_comment_act(request)

            return redirect(reverse('home'))

        else:
            func = process_attempt_rate(request)
            if func[1] == "nothing":
                messages.info(request, 'вы уже оценили это убожество')
                func = [func[0], "rate"]
            return JsonResponse({
                'rate': str(func[0].rate) if func[0] is not None else None,
                'action': func[1]
            })

# Social views


class ProfilePage(View):
    """ A class that is responsible for the profile page """
    def get(self, request):
        """ A function that processes get requests on the profile page """
        check_user(request)

        if 'attempt' in request.GET.keys():
            confirm_form = forms.AttemptForm()
            return render(request, 'snippets/confirm_completion_of_attempt.html', {'confirm_form': confirm_form})

        try:
            user_info = models.AdditionalInfo.objects.get(user=request.user)
        except (ObjectDoesNotExist, OperationalError):
            user_info = None

        try:
            uncompleted_attempts = models.Attempt.objects.filter(creator=request.user, is_completed=False)
        except ObjectDoesNotExist:
            uncompleted_attempts = []

        try:
            completed_attempts = models.Attempt.objects.filter(creator=request.user, is_completed=True)
        except ObjectDoesNotExist:
            completed_attempts = []

        if request.user.is_superuser:
            try:
                reports = models.Reports.objects.all()
            except ObjectDoesNotExist:
                reports = []

        else:
            reports = []

        try:
            all_relationships = models.FriendShip.objects.filter(from_friend=request.user)
            friends, subscriptions, subscribers = relationship_solver(all_relationships, request.user)

        except ObjectDoesNotExist:
            friends = None
            subscriptions = None
            subscribers = None

        if friends:
            friends_activity = []
            for friend in friends:
                try:
                    friend_info = models.AdditionalInfo.objects.get(user=friend.to_friend)
                    for objs in friend_info.get_activity:
                        friends_activity.append(objs)

                except ObjectDoesNotExist:
                    continue
        else:
            friends_activity = []

        context = {
            'activity': friends_activity[::-1],
            'user_info': user_info,
            'uncompleted_attempts': uncompleted_attempts,
            'completed_attempts': completed_attempts,
            'attempts': models.Attempt.objects.filter(creator=request.user),
            'achievements_completed': models.AchievementGot.objects.filter(user=request.user),
            'friends': friends,
            'subscriptions': subscriptions,
            'subscribers': subscribers,
            'reports': len(reports),
            'page_name': "Ваш профиль"
        }

        return render(request, 'pages/mobile_profile.html', context)

    def post(self, request):
        """ A function that processes post requests on the profile page"""
        if request.method == 'POST':
            if 'attempt-fill' in request.POST:
                confirm_form = forms.AttemptForm(request.POST, request.FILES)

                if confirm_form.is_valid():
                    attempt = models.Attempt.objects.get(id=request.POST['attempt'])
                    attempt.photo_second = confirm_form.cleaned_data['photo']
                    attempt.is_completed = True
                    attempt.save()

                    messages.info(request, 'Вы успешно завершили достижение.'
                                           'Теперь ожидайте оценки вашей попытки.')

                    return redirect(reverse('home'))
                else:
                    raise ValueError

            elif len(request.POST) == 2:
                process_attempt_delete(request)
                messages.info(request, 'Вы удалили свою попытку')

                return redirect(reverse('profile'))


class PersonProfilePage(View):
    """ A class that is responsible for other profile page"""

    def get(self, request, username):
        """ A function that processes get requests on the other profile page """
        check_user(request)

        clicked_user = models.User.objects.get(username=username)
        user_achieves, user_attempts, user_comments, users_avatar, friends, subscriptions, subscribers, user_achieves_got = get_user_info(clicked_user)

        if request.method == "GET":
            if 'report' in request.GET.keys():
                form = forms.ReportForm()
                return render(request, 'snippets/send_abuse.html', {'form': form})

        try:
            activity = models.AdditionalInfo.objects.get(user=clicked_user).get_activity
        except ObjectDoesNotExist:
            activity = []

        try:
            known_user = models.FriendShip.objects.get(from_friend=request.user, to_friend=clicked_user)
        except ObjectDoesNotExist:
            known_user = False

        try:
            user_info = models.AdditionalInfo.objects.get(user=clicked_user)
        except ObjectDoesNotExist:
            user_info = []

        context = {
            'achievements_completed': user_achieves_got,
            'comments': user_comments,
            'user': clicked_user,
            'user_info': user_info,
            'is_known': True if known_user else False,
            'friends': friends,
            'subscriptions': subscriptions,
            'subscribers': subscribers,
            'attempts': user_attempts,
            'activity': activity[::-1],
            'page_name': "Профиль пользователя",
        }

        return render(request, 'account/show_person.html', context)

    def post(self, request, username):
        """ A function that processes post requests on the other profile page """
        if request.method == "POST":
            if 'user' in request.POST['obj_type']:
                process_report(request)


# Achievements' page views

class AchievementListPage(View):
    """ A class that is responsible for page with list of achievements """

    def get(self, request):
        """ A function that processes get requests on the page with a list of achievements """
        check_user(request)

        content = {
            'achievements': models.Achievement.objects.all(),
            'form': forms.AchievementsSearchForm(),
            'page_name': "Достижения"
        }

        return render(request, 'pages/achievement_related/achievements_list.html', content)

    def post(self, request):
        """ A function that processes post requests on the page with a list of achievements"""
        if 'query' in request.POST.keys():
            form = forms.AchievementsSearchForm(request.POST)

            if form.is_valid():
                content = {
                    'achievements': search_achievements(form.data['query']),
                    'query': form.data['query'],
                    'page_name': "Достижения"
                }

                return render(request, 'pages/achievement_related/achievements_search_results.html', content)
        else:
            achievement = models.Achievement.objects.get(id=int(request.POST['achievement_id']))
            achievement.photo.delete()
            achievement.delete()

            return redirect(reverse('home'))


class UserLeaderboardPage(View):
    """ A class that is responsible for the ladder page of users """
    def get(self, request):
        """ A function that processes get requests on the ladder page"""
        check_user(request)

        return render(request, "pages/top_mobile.html")


class AchievementCreationPage(View):
    """ A class that is responsible for the page where user creates a new achievement """
    def get(self, request):
        """ Function that renders page where you can create an achievement """
        check_user(request)

        form_text = forms.AchievementForm()
        form_image = forms.SimpleImageUpload()

        context = {
            'form_text': form_text,
            'form_image': form_image,
            'messages': messages.get_messages(request),
            'page_name': "Cоздание достижения"
        }

        return render(request, 'pages/achievement_related/create_achievement.html', context)

    def post(self, request):
        """ A function that processes creation of an achievement """
        form_text = forms.AchievementForm(request.POST)
        form_image = forms.SimpleImageUpload(request.POST, request.FILES)

        if form_text.is_valid() and form_image.is_valid():
            # Step 1: basic validation
            caption, *tags = caption_handler(form_text.data['caption'])

            if validators.is_valid(caption):
                # Step 2: custom validation and if it's okay then we go ahead

                if ach_exists(caption):
                    # Step 3: if ach exists return with message otherwise create a new one
                    messages.info(request, 'Достижение уже существует. '
                                           'Вы можете <a href="/achievements/list/' + caption.lower() + '/">'
                                            'перейти</a> на него.'
                    )

                    self.get(request)

                else:
                    description = form_text.data['description']
                    photo = form_image.cleaned_data['photo']

                    try:
                        creator_info = models.AdditionalInfo.objects.get(user=request.user)
                    except ObjectDoesNotExist:
                        creator_info = None

                    achievement = models.Achievement(
                        creator=request.user,
                        creator_info=creator_info,
                        caption=caption,
                        difficulty=form_text.data['difficulty'],
                        description=description,
                        photo=photo
                    )

                    achievement.save()

                    for tag in tags:
                        models.AchievementsTag(
                            achievement=achievement,
                            name=tag,
                            creator=request.user
                        ).save()

                    return redirect(reverse('achievements'))

            else:
                messages.info(request, 'The name for the achievement is incorrect.')
                self.get(request)


class AchievementCheckPage(View):
    """ A class that is responsible for page where user creates an achievement """
    def get(self, request, caption):
        """ A function that processes get requests on the creation page """
        check_user(request)

        self.ach = models.Achievement.objects.get(caption=caption)

        try:
            attempts = models.Attempt.objects.filter(achievement=self.ach, is_completed=True)
        except ObjectDoesNotExist:
            attempts = []

        try:
            ach_tags = models.AchievementsTag.objects.filter(achievement=self.ach)
        except ObjectDoesNotExist:
            ach_tags = []

        if 'report' in request.GET.keys():
            attempt_comment_form = forms.AttemptRateForm()

            return render(request, 'snippets/send_attempt_comment.html', {'form': attempt_comment_form})

        comment_form = forms.AchievementRateForm()

        try:
            rates_data = models.AchievementRate.objects.filter(achievement=self.ach)
        except ObjectDoesNotExist:
            rates_data = []

        context = {
            'rates': rates_data,
            'achievement': self.ach,
            'tags': ach_tags,
            'form': comment_form,
            'page_name': "Просмотр достижения",
            'attempts': attempts
        }

        return render(request, 'pages/achievement_related/check_achievement.html', context)

    def post(self, request, caption):
        """ A function that processes post requests on the check achievement page """
        self.ach = models.Achievement.objects.get(caption=caption)

        if 'verdict' in dict(request.POST):
            func = process_attempt_rate(request)
            return JsonResponse({
                'rate': str(func[0].rate) if func[0] is not None else None,
                'action': func[1]
            })

        elif 'attempt_id' in request.POST.keys() and len(request.POST) == 2:
            process_attempt_delete(request)

        elif 'commentary' in request.POST.keys():
            comment_form = forms.AchievementRateForm(request.POST)

            if comment_form.is_valid():
                achievement_rate = models.AchievementRate(
                    achievement=self.ach,
                    commentary=comment_form.data['commentary'],
                    creator=request.user,
                )

                achievement_rate.save()

                return redirect('/achievements/list/' + self.ach.caption + '/')

        elif 'message' and 'attempt_id' in request.POST.keys():
            process_attempt_comment(request, self.ach.caption)

        elif request.POST['deletion'] == "True":
            self.ach.photo.delete()
            self.ach.delete()
            return redirect('/')


class AttemptCreationPage(View):
    """ A class that is responsible for page where user creates an attempt """
    def get(self, request, caption):
        """ Function that renders page where user puts selected achievement in 'to_complete_list' """
        check_user(request)

        self.ach = models.Achievement.objects.get(caption=caption)

        form = forms.AttemptForm()

        context = {
            'achievement_name': caption,
            'form': form
        }

        return render(request, "pages/achievement_related/create_attempt.html", context)

    def post(self, request, caption):
        form = forms.AttemptForm(request.POST, request.FILES)

        self.ach = models.Achievement.objects.get(caption=caption)
        self.creator_info = models.AdditionalInfo.objects.get(user=request.user)

        if form.is_valid():
            attempt = models.Attempt(
                creator=request.user,
                creator_info=self.creator_info,
                photo=form.cleaned_data["photo"],
                achievement=self.ach,
            )

            attempt.save()

            j = 1
            for i in range(1, len(request.POST) - 1):
                if request.POST['option' + str(i)]:
                    todo = models.AttemptTodoList(
                        attempt=attempt,
                        step='Пункт ' + str(j) + ': ' + request.POST['option' + str(i)]
                    )
                    todo.save()
                    j += 1

            messages.info(request, "Вы приступили к выполнению достижения " + self.ach.caption.lower() + ".")

        return redirect(reverse('achievements'))


class ReportPage(View):
    """ A class that is responsible for page with reports """
    def get(self, request):
        """ A function that processes get requests from the page """
        try:
            report = models.Reports.objects.all()
        except ObjectDoesNotExist:
            report = []

        if 'report' in request.GET.keys():
            form = forms.WarningForm()

            return render(request, 'snippets/report_ticket_solve.html', {'form': form})

        context = {
            'reports': report,
            'page_name': "Жалобы"
        }

        return render(request, 'pages/abuse/abuse_review.html', context)

    def post(self, request):
        if 'action' and 'type' and 'id' and 'report_id' in request.POST.keys():
            process_report_verdict(request)
            messages.info(request, 'Вы успешно решили жалобу.')

            return redirect(reverse('reports'))


class AboutPage(View):
    """ A class that is responsible for about page """
    def get(self, request):
        """ A function that processes get requests from the page """

        return render(request, 'pages/about.html')


# Change views


def change_comment(request, caption, commentary):
    """ Function that renders page where you can change your comment under an achievement """
    check_user(request)

    current_comment = models.AchievementRate.objects.get(commentary=commentary)
    related_achievement = models.Achievement.objects.get(caption=caption)

    if request.method == "POST":
        form = forms.ChangeCommentForm(request.POST)
        if form.is_valid():
            current_comment.commentary = form.data['comment']
            current_comment.rate = form.data['rate']
            current_comment.save()

            return redirect('/achievements/' + related_achievement.caption + '/')

    else:
        form = forms.ChangeCommentForm()

    content = {
        'form': form,
        'comment': current_comment,
        'achievement': related_achievement
    }

    return render(request, 'pages/change/comment_change.html', content)


class AchievementChangePage(View):
    """ A class that is responsible for page where user make adjustments to the achievement"""
    def get(self, request, caption):
        """ A function that processes get requests on the page """
        check_user(request)

        self.current_achievement = models.Achievement.objects.get(caption=caption)

        form_text = forms.AchievementChangeForm(
            initial={
                'caption': self.current_achievement.caption,
                'description': self.current_achievement.description,
                'difficulty': self.current_achievement.difficulty,
            }
        )

        form_image = forms.AchievementImageChangeForm(
            initial={
                'image': self.current_achievement.photo
            }
        )

        content = {
            'form_text': form_text,
            'form_image': form_image,
            'achievement': self.current_achievement

        }

        return render(request, 'pages/change/achievement_change.html', content)

    def post(self, request, caption):
        """ A function that processes post requests on the page """
        self.current_achievement = models.Achievement.objects.get(caption=caption)

        form_text = forms.AchievementChangeForm(request.POST)
        form_image = forms.AchievementImageChangeForm(request.POST, request.FILES)

        if form_text.is_valid() and form_image.is_valid():
            # Step 1: basic validation
            caption, *tags = caption_handler(form_text.data['caption'])

            if validators.is_valid(caption):
                # Step 2: custom validation and if it's okay then we go ahead
                self.current_achievement.description = form_text.data['description']
                self.current_achievement.caption = caption
                self.current_achievement.difficulty = form_text.data['difficulty']

                if form_image.cleaned_data['photo'] is not None:
                    self.current_achievement.photo = form_image.cleaned_data['photo']

                self.current_achievement.save()

                for tag in tags:
                    models.AchievementsTag(
                        achievement=self.current_achievement,
                        name=tag,
                        creator=request.user
                    ).save()

                return redirect('/achievements/list/' + self.current_achievement.caption + '/')


def change_profile(request):
    """ Function that renders page where user can change his personal information """
    check_user(request)

    try:
        user_info = models.AdditionalInfo.objects.get(user=request.user)
    except (ObjectDoesNotExist, OperationalError):
        user_info = None

    if request.method == "POST":
        form = forms.ChangeProfileForm(request.POST, request.FILES)

        if form.is_valid():

            me = request.user
            me.username = form.cleaned_data['username']
            me.save()

            if user_info is None and form.cleaned_data['avatar'] is not None:
                user_info = models.AdditionalInfo(
                    user=request.user,
                    img=form.cleaned_data['avatar'],
                )
                user_info.save()
                info_update(request.user, user_info)

            elif user_info is not None and form.cleaned_data['avatar'] is not None:
                user_info.img = form.cleaned_data['avatar']
                user_info.save()
                info_update(request.user, user_info)

            return redirect(reverse('profile'))

    else:
        form = forms.ChangeProfileForm(
            initial={
                'username': request.user.username
            }
        )

    content = {
        'form': form,
        'info': user_info,
        'page_name': 'Изменение профиля'
    }

    return render(request, 'pages/change/change_user_info.html', content)


# form funcs

def add_friend(request, username):
    clicked_user = models.User.objects.get(username=username)

    if request.method == "POST":
        try:
            models.FriendShip.objects.get(from_friend=request.user, to_friend=clicked_user)
            show_person(request, username)
        except ObjectDoesNotExist:
            friendship = models.FriendShip(from_friend=request.user, to_friend=clicked_user)

            try:
                friendship2 = models.FriendShip.objects.get(from_friend=clicked_user, to_friend=request.user)

                friendship.is_friend = True
                friendship2.is_friend = True

                friendship.is_sub = False
                friendship2.is_sub = False

                friendship.save()
                friendship2.save()
            except ObjectDoesNotExist:
                friendship.is_sub = True
                friendship.save()

    return redirect('/accounts/' + username + '/')


def remove_friend(request, username):
    clicked_user = models.User.objects.get(username=username)

    if request.method == "POST":
        friendship1 = models.FriendShip.objects.get(from_friend=request.user, to_friend=clicked_user)

        try:
            friendship2 = models.FriendShip.objects.get(from_friend=clicked_user, to_friend=request.user)
            friendship2.is_sub = True
            friendship2.is_friend = False
            friendship2.save()
        except ObjectDoesNotExist:
            friendship2 = []

        friendship1.delete()

    return redirect('/accounts/' + username + '/')


# help funcs


def process_attempt_rate(request):
    """
    helping function
    :param request: request
    :return: attempt
    """
    di = dict(request.POST)
    attempt_id = int(di["attempt_id"][0])
    verdict = int(di["verdict"][0])
    action = ''

    attempt = models.Attempt.objects.get(id=attempt_id)
    achievement = models.Achievement.objects.get(id=attempt.achievement.id)
    try:
        e = models.AttemptRate.objects.get(creator=request.user, attempt=attempt)
        if e and e.rate == -1:
            e.rate = verdict
            action = "rate"
            e.save()
        else:
            print(1)
            action = "nothing"

    except ObjectDoesNotExist:
        rate = models.AttemptRate(
            creator=request.user,
            rate=verdict,
            attempt=attempt
        )
        action = "rate"

        rate.save()

    if len(models.AttemptRate.objects.filter(attempt=attempt)) > 3:
        if attempt.rate > 50:
            ach_got = models.AchievementGot(
                user=attempt.creator,
                achievement=achievement
            )

            try:
                user_info = models.AdditionalInfo.objects.get(user=request.user)
                user_info.exp = user_info.exp + achievement.exp
                user_info.save()

            except ObjectDoesNotExist:
                user_info = models.AdditionalInfo(
                    user=attempt.creator,
                    exp=achievement.exp
                )
                user_info.save()

            achievement.exp = achievement.exp - 5

            ach_got.save()
            achievement.save()
            attempt.photo.delete(save=True)
            attempt.delete()
            action = "delete"

            return None, action

    return attempt, action


def process_attempt_delete(request):
    """ A function that is used to delete an attempt """
    attempt_id = request.POST['attempt_id']
    attempt = models.Attempt.objects.get(id=int(attempt_id))
    attempt.photo.delete()

    if attempt.photo_second:
        attempt.photo_second.delete()

    attempt.delete()


def process_report(request):
    """ A function that is used to create a report """
    obj_type = request.POST['obj_type']
    id = request.POST['type-id']

    form = forms.ReportForm(request.POST)

    if form.is_valid():
        if obj_type == "attempt":
            obj = models.Attempt.objects.get(id=int(id))

        elif obj_type == "achievement":
            obj = models.Achievement.objects.get(id=int(id))

        elif obj_type == "user":
            obj = models.AdditionalInfo.objects.get(user=models.User.objects.get(id=int(id)))

        report = models.Reports(
            from_who=request.user,
            content_object=obj,
            message=form.data['message'],
            what_am_i=obj_type,
        )

        report.save()


def process_report_verdict(request):
    """ A function that is used to solve reports """

    action, obj_type = request.POST['action'], request.POST['type']
    obj_id, report_id = request.POST['id'], request.POST['report_id']

    form = forms.WarningForm(request.POST)

    if form.is_valid():
        message = form.data['message'] if form.data['message'] == ' ' else None

        if action == 'leave':
            report = models.Reports.objects.get(id=int(report_id))
            report.delete()

        else:
            if obj_type == 'attempt':
                attempt = models.Attempt.objects.get(id=int(obj_id))
                models.Reports.objects.get(id=int(report_id)).delete()

                add_warn(attempt.creator, message)
                attempt.photo.delete()
                attempt.photo_second.delete()
                attempt.delete()

            elif obj_type == 'achievement':
                achievement = models.Achievement.objects.get(id=int(obj_id))
                models.Reports.objects.get(id=int(report_id)).delete()

                add_warn(achievement.creator, message)
                achievement.photo.delete()
                achievement.delete()

            elif obj_type == 'user':
                models.Reports.objects.get(id=int(report_id)).delete()

                add_warn(models.AdditionalInfo.objects.get(id=int(obj_id)).user, message)

            else:
                models.Reports.objects.get(id=int(report_id)).delete()
                attempt_comment = models.AttemptRate.objects.get(id=int(obj_id))
                attempt_comment.commentary = None
                attempt_comment.save()


def process_attempt_comment(request, caption):
    """ A function that is used to create commentary under attempts """
    attempt_comment_form = forms.AttemptRateForm(request.POST)
    ach = models.Achievement.objects.get(caption=caption)

    if attempt_comment_form.is_valid():
        attempt = models.Attempt.objects.get(id=int(request.POST['attempt_id']))

        try:
            attempt_rate = models.AttemptRate.objects.get(attempt=attempt, creator=request.user)
            attempt_rate.commentary = attempt_comment_form.data['message']
            attempt_rate.save()

        except ObjectDoesNotExist:
            attempt_rate = models.AttemptRate(
                attempt=attempt,
                creator=request.user,
                commentary=attempt_comment_form.data['message']
            )
            attempt_rate.save()

        return redirect('/achievements/list/' + ach.caption + '/')


def process_attempt_comment_act(request):
    """ A function that manipulates attempt comments """
    comment = models.AttemptRate.objects.get(id=int(request.POST['id']))

    if request.POST['action'] == "delete":
        comment.commentary = None
        comment.save()

    else:
        report = models.Reports(
            content_object=comment,
            from_who=request.user,
            what_am_i="attempt_comment",
        )

        report.save()


def get_user_info(clicked_user):
    try:
        user_achieves = models.Achievement.objects.filter(creator=clicked_user)
    except ObjectDoesNotExist:
        user_achieves = []
    user_achieves_got = models.AchievementGot.objects.filter(user=clicked_user)
    try:
        user_attempts = models.Attempt.objects.filter(creator=clicked_user, is_completed=True)
    except ObjectDoesNotExist:
        user_attempts = []

    try:
        user_comments = models.AchievementRate.objects.filter(creator=clicked_user.id)
    except ObjectDoesNotExist:
        user_comments = []

    try:
        users_avatar = models.AdditionalInfo.objects.get(user=clicked_user)
    except (ObjectDoesNotExist, OperationalError):
        users_avatar = []

    try:
        all_relationships = models.FriendShip.objects.filter(from_friend=clicked_user)
        friends, subscriptions, subscribers = relationship_solver(all_relationships, clicked_user)
    except ObjectDoesNotExist:
        friends = []
        subscriptions = []
        subscribers = []
    return [user_achieves, user_attempts, user_comments, users_avatar, friends, subscriptions, subscribers, user_achieves_got]


def relationship_solver(relations, user):
    """ Function that separates subscriptions and friends and subscribers """
    friends = []
    subscriptions = []
    subscribers = []

    for i in relations:
        if i.is_sub:
            subscriptions.append(i)

        elif i.is_friend:
            friends.append(i)

    try:
        subers = models.FriendShip.objects.filter(to_friend=user)

        for i in subers:
            if i.is_sub:
                subscribers.append(i)

    except ObjectDoesNotExist:
        subscribers = None

    return friends, subscriptions, subscribers


def caption_handler(caption):
    """ Function that handles caption and puts tags """
    stroke = caption.split('#')

    for index, item in enumerate(stroke):
        stroke[index] = item.strip()

    return stroke


def ach_exists(caption):
    try:
        _existing_ach = models.Achievement.objects.get(caption=caption)
        return True
    except ObjectDoesNotExist:
        return False


def search_achievements(query):
    results = models.AchievementsTag.objects.filter(
        Q(name__icontains=query)
    )

    return results


def check_user(request):
    """ A function that checks warns of the user"""
    try:
        info = models.AdditionalInfo.objects.get(user=request.user)
        if info.warnings > 3:
            info.img.delete(save=True)
            request.user.delete()
            messages.info(request, 'ВАШ АККАУНТ БЫЛ УДАЛЕН В СВЯЗИ ТЕМ, ЧТО ВЫ ПОЛУЧИЛИ СЛИШКОМ МНОГО ПРЕДУПРЕЖДЕНИЙ')

    except ObjectDoesNotExist:
        info = models.AdditionalInfo(
            user=request.user
        )

        info.save()


def add_warn(user, message):
    """ A function that adds a warn for a user"""
    warn = models.ReportWarning(
        user=user,
        reason=message
    )
    warn.save()

    user_info = models.AdditionalInfo.objects.get(user=user)
    user_info.warnings = user_info.warnings + 1
    user_info.save()


def info_update(user, user_info):
    """ Function that changes creator_info in achievement and attempts which he has created """
    try:
        achievements = models.Achievement.objects.filter(creator=user)
    except ObjectDoesNotExist:
        achievements = None

    try:
        attempts = models.Attempt.objects.filter(creator=user)
    except ObjectDoesNotExist:
        attempts = None

    for i in achievements:
        i.creator_info = user_info
        i.save()

    for i in attempts:
        i.creator_info = user_info
        i.save()


def get_user_exp(user):
    """
    helping func
    :param user: User
    :return: user's exp
    """
    return sum(map(lambda i: i.achievement.exp, models.AchievementGot.objects.filter(user=user)))


def top_users(request):
    a = models.User.objects.all()
    context = {
        "users": zip(sorted(a, key=lambda x: get_user_exp(x)), map(lambda t: get_user_exp(t), a))
    }
    return render(request, 'pages/top_users.html', context)


def top_achievements(request):
    a = models.Achievement.objects.all()
    context = {
        "achievements": sorted(a, key=lambda x: x.exp)
    }
    return render(request, 'pages/top_achievements.html', context)