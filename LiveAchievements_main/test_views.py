from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.core.files.uploadedfile import SimpleUploadedFile
from os import remove
from . import forms, models


class ViewsTest(TestCase):
    """ Class that tests views"""
    @classmethod
    def setUpClass(cls) -> None:
        User.objects.create(username='User', password='KeKus333', email='kek@kek.ru')
        cls.user = User.objects.get(id=1)
        cls.c = Client()
        cls.c.get('/accounts/login/')
        cls.c.force_login(cls.user)

    def test_ach_create_GET(self) -> None:
        """ A method that verifies the correctness of the achievement list page """
        response = self.c.get('/achievements/create/')

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/achievement_related/create_achievement.html')
        self.assertIsNotNone(response.context['form_text'])
        self.assertIsNotNone(response.context['form_image'])
        self.assertIsNotNone(response.context['page_name'])

    def test_ach_create_POST(self) -> None:
        """ A method that verifies the proper creation of an achievement """
        url = '/achievements/create/'

        photo_list = image_loader()
        response = self.c.post(url, {
            'caption': "Test",
            'description': "This is a good test.",
            'photo': photo_list[0],
            'days': 1,
            'hours': 1,
            'minutes': 1
        })

        self.assertEqual(response.status_code, 302)

        response = self.c.post(url, {
            'caption': '$*()%&#)(@#_)',
            'description': 'Kej',
            'photo': photo_list[1],
            'days': 1,
            'hours': 1,
            'minutes': 1
        })

        self.assertEqual(response.status_code, 200)

        response = self.c.post(url, {
            'caption': "testing test #test",
            'description': "This is a good test.",
            'photo': photo_list[2],
            'days': 1,
            'hours': 1,
            'minutes': 1
        })

        self.assertEqual(response.status_code, 302)

        response = self.c.post(url, {
            'caption': "ab ",
            'description': 'Kklfjghdkl',
            'photo': photo_list[3],
            'days': 1,
            'hours': 1,
            'minutes': 1
        })

        self.assertEqual(response.status_code, 302)

        response = self.c.post(url, {
            'caption': 'abc #da',
            'description': "DROP-TABLE",
            'photo': photo_list[4],
            'days': 1,
            'hours': 1,
            'minutes': 1
        })

        self.assertEqual(response.status_code, 302)

        response = self.c.post(url, {
            'caption': 'abc #da #%*($)',
            'description': "DROP TABLE db.sqlite.3",
            'photo': photo_list[5],
            'days': -1,
            'hours': -2,
            'minutes': -3,
        })

        self.assertEqual(response.status_code, 200)

        response = self.c.post(url, {
            'caption': 'abc #da #%*($)',
            'description': "DROP TABLE db.sqlite.3",
            'photo': photo_list[6],
            'days': 99999999999999999999999999999,
            'hours': 999999999999999999999999999999999999999999999,
            'minutes': 999999999999999999999999999,
        })

        self.assertEqual(response.status_code, 200)

        response = self.c.post(url, {
            'caption': 'testing_тестим_тес #каk',
            'description': "тестирующий тест тестировщик",
            'photo': photo_list[7],
            'days': 0,
            'hours': 1,
            'minutes': 1
        })

        self.assertEqual(response.status_code, 302)

        response = self.c.post(url, {
            'caption': '__init__self',
            'description': 'lfkgjhd',
            'photo': photo_list[7],
            'days': 100,
            'hours': 100,
            'minutes': 100
        })

        self.assertEqual(response.status_code, 200)

        response = self.c.post(url, {
            'caption': '__init__self',
            'description': 'lfkgjhd',
            'photo': photo_list[8],
            'days': 100,
            'hours': 100,
            'minutes': 100
        })

        self.assertEqual(response.status_code, 302)

        response = self.c.post(url, {
            'caption': 'supr1se&action',
            'description': 'lfkgjhd',
            'photo': photo_list[9],
            'days': 1,
            'hours': 1,
            'minutes': 0
        })

        self.assertEqual(response.status_code, 200)

    def test_ach_list_GET(self) -> None:
        """ A method that verifies the correctness of the achievement list page """
        response = self.c.get('/achievements/list/')

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/achievement_related/achievements_list.html')
        self.assertIsNotNone(response.context['form'])
        self.assertIsNotNone(response.context['page_name'])

        # Every function starts with empty db?

    def test_profile_GET(self) -> None:
        """ A method that verifies the correctness of the profile page """
        response = self.c.get('/accounts/profile/')

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/mobile_profile.html')
        self.assertIsNotNone(response.context['form'])
        self.assertIsNotNone(response.context['attempts'])
        self.assertIsNotNone(response.context['attempts_completed'])
        self.assertIsNotNone(response.context['friends'])
        self.assertIsNotNone(response.context['subscriptions'])
        self.assertIsNotNone(response.context['subscribers'])

    @classmethod
    def tearDownClass(cls) -> None:
        for i in range(10):
            try:
                remove('./media/achievements_photos/test_ach_' + str(i + 1) + '.jpg')
            except FileNotFoundError:
                continue

# helpers


def image_loader():
    photo_list = []

    for i in range(10):
        photo = SimpleUploadedFile(
            name='test_ach_' + str(i + 1) + '.jpg',
            content=open('./images_for_test/views/ach_create_' + str(i + 1) + '.jpg', 'rb').read(),
            content_type='image/jpg'
        )

        photo_list.append(photo)

    return photo_list
