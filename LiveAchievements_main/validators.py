from django.core.validators import RegexValidator


def caption_valid(value):
    """ Manual validation for caption ("""
    caption_validator = RegexValidator(
        regex='^[(?u)\w+\s+\.+\@+\%+\&+\*+\-+\++\=+\|+]*$',
        message='Custom validation error'
        )

    try:
        caption_validator(value)
        return True

    except:
        return False


def is_valid(value):

    return True if caption_valid(value) else False
