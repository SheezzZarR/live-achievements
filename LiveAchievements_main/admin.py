from django.contrib import admin

# Register your models here.
from .models import Achievement, AchievementGot, AchievementsTag, AchievementRate, Attempt, AttemptRate

admin.site.register(Achievement)
admin.site.register(AchievementGot)
admin.site.register(AchievementsTag)
admin.site.register(AchievementRate)
admin.site.register(Attempt)
admin.site.register(AttemptRate)