from django import forms

CHOISES = [
    (1, 'Легко'),
    (2, 'Трудно'),
    (3, 'Очень трудно')
]


class AchievementForm(forms.Form):
    """ A Form that is used to create an achievement """
    caption = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': "form-control add-ach-inp",
                'id': "inutTitle",
                'placeholder': "Введите название"

            }
        ),
    )

    description = forms.CharField(
        max_length=300,
        widget=forms.Textarea(
            attrs={
                'class': "add-ach-inp add-ach-inp-opis",
                'rows': 5,
                'placeholder': "Введите описание"
            }
        )
    )

    difficulty = forms.ChoiceField(
        required=True,
        widget=forms.RadioSelect(
            attrs={
                'class': "radio-inline",
                'name': 'optradio'
            }
        ),
        choices=CHOISES
    )


class AchievementRateForm(forms.Form):
    """ A form that is used to rate an achievement """
    commentary = forms.CharField(
        max_length=300,
        widget=forms.Textarea(
            attrs={
                'class': "add-ach-inp add-ach-inp-opis",
                'rows': 3,
                'placeholder': "Введите комментарий"
            }
        )
    )


class SimpleImageUpload(forms.Form):
    """ A form that is used to import an image """
    photo = forms.ImageField(label="Выберите изображение")


class AttemptForm(forms.Form):
    """ A form that is used to create a photo for the attempt """
    photo = forms.ImageField(label="Выберите изображение")


class AttemptRateForm(forms.Form):
    """ A form that is used to create a comment to the attempt """
    message = forms.CharField(
        required=True,
        max_length=200,
        widget=forms.Textarea(
            attrs={
                'class': "add-ach-inp add-ach-inp-opis",
                'rows': 3,
                'name': "description",
                'placeholder': "Введите текст комментария"
            }
        )
    )


class AchievementsSearchForm(forms.Form):
    """ A form that is used to search achievements """
    query = forms.CharField(label="Поиск достижений по тегами", required=True, max_length=300)


class ReportForm(forms.Form):
    """ A form that is used to send a report about smth """
    message = forms.CharField(
        required=True,
        max_length=500,
        widget=forms.Textarea(
            attrs={
                'class': "add-ach-inp add-ach-inp-opis",
                'rows': 4,
                'name': "description",
                'placeholder': "Введите текст жалобы"
            }
        )
    )


class WarningForm(forms.Form):
    """ A form that is used to add messages to tickets on the page with reports """
    message = forms.CharField(
        required=False,
        max_length=500,
        widget=forms.Textarea(
            attrs={
                'class': "add-ach-inp add-ach-inp-opis",
                'rows': 5,
                'name': "description",
                'placeholder': "Введите текст"
            }
        )
    )


# Change forms


class ChangeCommentForm(forms.Form):
    """ A form that is used to change comment """
    CHOICES = [(i, str(i)) for i in range(1, 6)]

    comment = forms.CharField(label="Ваш комментарий", max_length=300)
    rate = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)


class AchievementChangeForm(forms.Form):
    """ A form that is used to change an achievement """
    caption = forms.CharField(
        required=False,
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': "form-control add-ach-inp",
                'id': "inutTitle",
                'name': "caption"

            }
        ),
    )

    description = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(
            attrs={
                'class': "add-ach-inp add-ach-inp-opis",
                'rows': 5,
                'name': "description"
            }
        )
    )

    difficulty = forms.ChoiceField(
        required=True,
        widget=forms.RadioSelect(
            attrs={
                'class': "radio-inline",
                'name': 'optradio'
            }
        ),
        choices=CHOISES
    )


class AchievementImageChangeForm(forms.Form):
    photo = forms.ImageField(
        label="Другое изображение",
        required=False,
        widget=forms.FileInput(
            attrs={
                'name': "image"
            }
        )
    )


class ChangeProfileForm(forms.Form):
    username = forms.CharField(
        label="Новый ник: ",
        required=False,
        widget=forms.TextInput(
            attrs={
                'name': "username"
            }
        )
    )
    avatar = forms.ImageField(label="Изменить аватар", required=False)
